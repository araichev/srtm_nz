"""
Python 3 code that checks whether all SRTM1 and SRTM3 data is present for the tiles covering New Zealand.
"""
import os
from pathlib import Path
import json


PROJECT_ROOT = Path(os.path.abspath(os.path.join(
  os.path.dirname(__file__), '.')))
GEOJSON_PATH = PROJECT_ROOT/'srtm_nz_polygons.geojson'

def check_files():
    # Load SRTM NZ polygons
    with GEOJSON_PATH.open() as src:
        tiles = json.load(src)

    # For each polygon, check that its SRTM1 and SRTM3 data exists and lies
    # in the proper place
    expect_tile_ids = [f['properties']['tile_id'] for f in tiles['features']]
    failed = False
    for t in expect_tile_ids:
        for f in [PROJECT_ROOT/'srtm1'/'{!s}.SRTMGL1.hgt.zip'.format(t),
          PROJECT_ROOT/'srtm3'/'{!s}.SRTMGL3.hgt.zip'.format(t)]:
            if not f.exists():
                print('Missing tile {!s}'.format(f.stem))
                failed = True

    if not failed:
        print('All SRTM1 and SRTM3 data for the tiles listed in {!s} is present.'.format(GEOJSON_PATH))


if __name__ == '__main__':
    check_files()