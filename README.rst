SRTM NZ
*********
Version 3.0 Shuttle Radar Topography Mission (SRTM) digital surface model data for New Zealand.

In this repository you will find

- All SRTM3 tiles (3 arc second resolution; roughly 66 meters at 45 degrees south latitude) covering New Zealand and sourced from from NASA from http://e4ftl01.cr.usgs.gov/SRTM/SRTMGL3.003/2000.02.11
- All SRTM1 tiles (1 arc second resolution; roughly 22 meters at 45 degrees south latitude) covering New Zealand and sourced from NASA from http://e4ftl01.cr.usgs.gov/SRTM/SRTMGL1.003/2000.02.11
- A GeoJSON file of the SRTM tiles that cover New Zealand. Useful for a visual check using, say, `geojson.io <http://geojson.io/#id=gist:anonymous/81b4cb465f1c78941f665c9038494f0f&map=5/-41.360/172.463>`_.
- A Python 3 script that checks whether all SRTM1 and SRTM3 data is present for the tiles listed in the GeoJSON file


License
========
The content of this repository is licensed under a `Creative Commons Zero 1.0 License (CC0 1.0) <https://creativecommons.org/publicdomain/zero/1.0/>`_.


Further Reading
================
- `Open Street Map wiki page about SRTM <Shuttle Radar Topography Mission (SRTM)>`_